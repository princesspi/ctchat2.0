
<!DOCTYPE html>
<html>
  <head>
    <title>CTCHAT2.0 Chat</title>
    <meta charset="utf-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!--favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    
    <link rel="stylesheet" href="css/main.css">
	
	<!-- SCRIPTS -->
	<script src="/includes/openpgpjs/openpgp.min.js></script>
  </head>
  
  <body>
    <div id="main">
      <h1>Anonymous Secure Chat</h1>
      <h2>Login Securely</h2>
      <form id="login" action="exec/exec_login.php" method="post">
        <dl>
          <dt>
            <label for="username">Username</label>
          </dt>
          <dd>
            <input type="text" id="username" name="username"  maxlength="60"> <input type="checkbox" checked="checked" id="guestbox"> <label for="guestbox">Guest?</label>
          </dd>
          
          <dt>
            <label for="password">Password</label>
          </dt>
          <dd>
            <input type="password" name="password">
          </dd>
          
          <dt>
            <input type="submit" value="Log In" class="login">
          </dt>
          
          <dt><a href="register.php">Register an Account</a></dt>
        </dl>
        
      </form>
    </div>
  </body>